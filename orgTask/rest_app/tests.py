import json
from rest_framework import status
from django.test import TestCase, Client
from django.urls import reverse
from django.db import IntegrityError

from . import views
from django.contrib.auth.models import User, Group
from organizations.utils import create_organization
from organizations.models import Organization, OrganizationUser
from .serializers import UserSerializer, OrganizationSerializer, OrganizationUserSerializer

username = 'test_ruby'
password = 'abc@123'
email    = 'test@python.com'
my_admin = None

# try:
#   my_admin = User.objects.create_user(username=username,email=email,password=password,is_staff=True,is_active=True,is_superuser=True)
#   my_admin.save()
# except IntegrityError as e:
#   my_admin = User.objects.get(username=username)

# client = Client()

# print('IS_SUPERUSER :: ' + str(my_admin.is_superuser))
# print('LOGGED_IN :: ' + str(res))

class AuthenticatedUsers(TestCase):
  # def test_without_authentication(self):
  #   response = self.client.get('/api/users')
  #   self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

  def test_without_authentication(self):
    response = self.client.get('/api/users/')
    self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class GetAllUsers(TestCase):
  """ Test module for GET all users API """
  my_admin = None

  def setUp(self):
    try:
      my_admin = User.objects.create_user(username=username,email=email,password=password,is_staff=True,is_active=True,is_superuser=True)
    except IntegrityError as e:
      my_admin = User.objects.get(username=username)
    
    my_admin.save()

    User.objects.create(username='Casper', email="Casper@gmail.com", password="ruby@123")
    User.objects.create(username='Muffin', email="Muffin@gmail.com", password="ruby@123")
    User.objects.create(username='Rambo', email="Rambo@gmail.com", password="ruby@123")
    User.objects.create(username='Ricky', email="Ricky@gmail.com", password="ruby@123")

  def test_get_users(self):
    self.client.login(username=username,password=password)
    response = self.client.get('/api/users/?format=json')
    users = User.objects.all()
    serializer = UserSerializer(users, 'users')
    serializer.is_valid()
    self.assertEqual(response.status_code, 200)




