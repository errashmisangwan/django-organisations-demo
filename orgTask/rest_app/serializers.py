from django.contrib.auth.models import User, Group
from organizations.models import Organization, OrganizationUser
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
  class Meta:
    model = User
    fields = ('url', 'id', 'username', 'email', 'groups')


class GroupSerializer(serializers.ModelSerializer):
  class Meta:
    model = Group
    fields = ('url', 'name')


class OrganizationSerializer(serializers.ModelSerializer):
  class Meta:
    model = Organization
    fields = ('url', 'id', 'name', 'is_active')


class OrganizationUserSerializer(serializers.ModelSerializer):
  class Meta:
    model = OrganizationUser
    fields = '__all__'