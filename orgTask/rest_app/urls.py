from django.urls import path
from django.conf.urls import url, include
from . import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)
router.register(r'organizations', views.OrganizationViewSet)
router.register(r'organization_users', views.OrganizationUserViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),
    path('add_user', views.add_user, name = 'add_user'),
]