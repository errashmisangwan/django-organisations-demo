from organizations.models import Organization, OrganizationUser
from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from rest_app.serializers import UserSerializer, GroupSerializer, OrganizationSerializer, OrganizationUserSerializer

from django.http import HttpResponse 
from django.contrib.auth.decorators import login_required
from django.db import IntegrityError

# from organizations.utils import create_organization

# Create your views here.
@login_required
def add_user(request):
  curr_user       = request.user
  curr_orgs       = curr_user.organizations_organization.all()
  new_user_name   = None
  new_user_email  = None
  new_user_pass   = None
  org_name        = None
  selected_org    = None
  is_admin        = False

  if request.method != 'GET':
    return HttpResponse("Only GET or POST method supported for adding user")

  new_user_name   = request.GET.get('user_name')
  new_user_email  = request.GET.get('user_email')
  new_user_pass   = request.GET.get('user_pass')
  org_name        = request.GET.get('org_name')
  is_admin        = request.GET.get('is_admin')

  if not is_admin:
    is_admin = False

  if not new_user_name:
    return HttpResponse("Username not provided")
  elif not new_user_email:
    return HttpResponse("Email not provided")
  elif not new_user_pass:
    return HttpResponse("Password not provided")
  elif not org_name:
    return HttpResponse("Organisation Name not provided")

  print(curr_user)
  print(curr_orgs)

  for org in curr_orgs:
    if org_name == org.name:
      selected_org = org

  if not selected_org:
    return HttpResponse("Current User is not authorised for organization: " + org_name)

  if not selected_org.is_admin(curr_user):
    return HttpResponse("Current User is not admin")

  try:
    new_user = User.objects.get(username=new_user_name)
    print('Existing User')
  except User.DoesNotExist:
    print('New User')
    new_user = User.objects.create_user(new_user_name, new_user_email, new_user_pass)

  if not new_user:
    return HttpResponse("Error creating user: " + new_user_name)

  try:
    selected_org.add_user(new_user, is_admin=is_admin)
    return HttpResponse("User <b>" + new_user.username + "</b> is successfully added to <b>" + selected_org.name + "</b> by <b>" + curr_user.username + "</b>")
  except IntegrityError as e:
    return HttpResponse(e.__cause__)


class UserViewSet(viewsets.ModelViewSet):
  """
  API endpoint that allows users to be viewed or edited.
  """
  queryset = User.objects.all()
  serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
  """
  API endpoint that allows groups to be viewed or edited.
  """
  queryset = Group.objects.all()
  serializer_class = GroupSerializer


class OrganizationViewSet(viewsets.ModelViewSet):
  """
  API endpoint that allows groups to be viewed or edited.
  """
  queryset = Organization.objects.all()
  serializer_class = OrganizationSerializer


class OrganizationUserViewSet(viewsets.ModelViewSet):
  """
  API endpoint that allows groups to be viewed or edited.
  """
  queryset = OrganizationUser.objects.all()
  serializer_class = OrganizationUserSerializer
