from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('organizations.urls')),
    path('api/', include('rest_app.urls')),
]
